package com.stach;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class OspAplicationTwoApplication {

	public static void main(String[] args) {
		SpringApplication.run(OspAplicationTwoApplication.class, args);
	
	}
}
