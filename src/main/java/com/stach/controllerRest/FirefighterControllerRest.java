package com.stach.controllerRest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stach.model.Firefighter;
import com.stach.repository.FirefighterRepository;

@RestController
@RequestMapping("/api/firefighter")
public class FirefighterControllerRest {

	@Autowired
	private FirefighterRepository firefighterRepository;
	
	
	@GetMapping(path ="/{city}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Firefighter> getFirefightersByCity(@PathVariable String city){
		List<Firefighter> firefighters = firefighterRepository.findByUnit_City(city);
		return firefighters;
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Firefighter> getFirefighters(){
		List<Firefighter> firefighters = firefighterRepository.findAll();
		return firefighters;
	}
}
