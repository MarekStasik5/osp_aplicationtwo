package com.stach.controllerRest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stach.model.User;
import com.stach.repository.UserRepository;

@RestController
@RequestMapping("/api/user")
public class UserControllerRest {
	
	private UserRepository userRepository;

	@Autowired
	public UserControllerRest(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@GetMapping(path = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
	public  String login(@PathVariable String username) {
		if(userRepository.findByUsername(username)!=null) {
			return "success";
    	}
		else 
			return "failed";
		
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public void saveUser(@RequestBody User user) {
		userRepository.save(user);
	}

}
