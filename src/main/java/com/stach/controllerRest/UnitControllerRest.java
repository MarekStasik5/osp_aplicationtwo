package com.stach.controllerRest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stach.model.Unit;
import com.stach.repository.UnitRepository;

@RestController
@RequestMapping("/api/city")
public class UnitControllerRest {

	private UnitRepository unitRepository;

	@Autowired
	public UnitControllerRest(UnitRepository unitRepository) {
		this.unitRepository = unitRepository;
	}
	
	@GetMapping(path ="/{truck}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Unit> getCityByTruck(@PathVariable String truck) {
		List<Unit> units=unitRepository.findByTruck(truck);
		return units;
	}
	
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public void saveUnit(@RequestBody Unit unit) {
		unitRepository.save(unit);
	}
	
	 @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<Unit> getUnits(){
		 List<Unit> units=unitRepository.findAll();
		 return units;
	 }
	
}
