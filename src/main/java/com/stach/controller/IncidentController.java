package com.stach.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.stach.model.Firefighter;
import com.stach.model.Incident;
import com.stach.repository.FirefighterRepository;
import com.stach.repository.IncidentRepository;


@Controller
@RequestMapping("/incident")
public class IncidentController {
	
	@Autowired
	private FirefighterRepository firefighterRepository;
	@Autowired
	private IncidentRepository incidentRepository;

	@GetMapping("/addIncident")
	public String addIncident(Model model) {
		model.addAttribute("incident", new Incident());
		return "addIncident";
	}
	
	@GetMapping("/saveIncident")
    public String saveIncident(@ModelAttribute Incident incident,@RequestParam String lastName) {
		incidentRepository.save(incident);
		compareFirefighterAndIncident(lastName, incident);
		return "success";
	} 
	
	public void compareFirefighterAndIncident(String lastName, Incident incidentTMP) {
		Firefighter firefigherTMP = firefighterRepository.findByLastName(lastName);
		Set<Incident>incidents=new HashSet<Incident>();
		incidents.add(incidentTMP);
		firefigherTMP.setIncidents(incidents);
		firefighterRepository.save(firefigherTMP);
		
		Set<Firefighter>firefighters=new HashSet<Firefighter>();
		firefighters.add(firefigherTMP);
		firefigherTMP.setIncidents(incidents);
		incidentRepository.save(incidentTMP);		
	}
	
}
