package com.stach.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.stach.model.Unit;
import com.stach.repository.UnitRepository;

@Controller
@RequestMapping("/unit")
public class UnitController {

	private UnitRepository unitRepository;

	@Autowired
	public UnitController(UnitRepository unitRepository) {
		this.unitRepository = unitRepository;
	}
	
	@GetMapping("/addUnit")
	public String addUnit(Model model) {
		model.addAttribute("unit", new Unit());
		return "addUnit";	
	}
	
	@GetMapping("/showAllUnits")
	public String showUnits(Model model) {
		List<Unit> allUnits=unitRepository.findAll();
		model.addAttribute("allUnits", allUnits);
		return "showUnits";
	}
	
	@GetMapping("/saveUnit")
	public String addUnit(@ModelAttribute Unit unit) {
		unitRepository.save(unit);
		return "success";		
	}
	
}
