package com.stach.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.stach.model.Firefighter;
import com.stach.model.Unit;
import com.stach.model.User;
import com.stach.repository.FirefighterRepository;
import com.stach.repository.UnitRepository;
import com.stach.repository.UserRepository;


@Controller
@RequestMapping("/firefighter")
public class FirefighterController {

	@Autowired
	private FirefighterRepository firefighterRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UnitRepository unitRepository;

	@GetMapping("/addFirefighter")
    public String addFirefighter(Model model) {
		model.addAttribute("firefighter", new Firefighter());
		return "addFirefighter";
	}

	@GetMapping("/saveFirefighter")
    public String saveFirefighter(@ModelAttribute Firefighter firefighter, @RequestParam String username, @RequestParam String city) {
		User userTMP = userRepository.findByUsername(username);
		Unit unitTMP= unitRepository.findByCity(city);
		firefighter.setUser(userTMP);
		firefighter.setUnit(unitTMP);
		firefighterRepository.save(firefighter);
		return "success";
	}
	
	@GetMapping("/deleteFirefighter")
	public String deleteFirefighter() {		
		return "deleteFirefighter";
	}
	
	@GetMapping("/deleteFirefighterByLastName")
	public String deleteFirefighterByLastName(@RequestParam String lastName) {
		Firefighter firefighterTMP = firefighterRepository.findByLastName(lastName);
		firefighterRepository.deleteById(firefighterTMP.getIdFirefighter());		
		return "success";
	}		
	
}
