package com.stach.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.stach.model.Firefighter;
import com.stach.model.Unit;
import com.stach.model.User;
import com.stach.repository.FirefighterRepository;
import com.stach.repository.UnitRepository;
import com.stach.repository.UserRepository;

@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UnitRepository unitRepository;
	@Autowired
	private FirefighterRepository firefighterReposiotry;
	
	
	@PostMapping("/saveUser")
    public String saveUser(@ModelAttribute User user) {
        userRepository.save(user);
        return "redirect:/";
    }

    @GetMapping("/showUsers")
    public String showAll(Model model) {
        List<User> allUsers = userRepository.findAll();
        model.addAttribute("allUsers", allUsers);
        
        List<Unit>allUnits=unitRepository.findAll();
        model.addAttribute("allUnits", allUnits);
        
        String profession="firefighter";
        List<Firefighter>selectedFirefighters=firefighterReposiotry.showByFireProfession(profession);
        model.addAttribute("selectedFirefighters",selectedFirefighters);
        
        String city="wawa";
        List<Firefighter>firefighterInUnit=firefighterReposiotry.findByUnit_City(city);
        model.addAttribute("firefighterInUnit",firefighterInUnit);

        return "showAll";
    }
    
    @GetMapping("/register")
	public String addUser(Model model) {
		model.addAttribute("user", new User());
		return "register";
	}


    @GetMapping("/login")
	public String login(Model model) {
		model.addAttribute("user", new User());
		return "login";
	}
    
    @PostMapping("/tryLogin")
    public String tryLogin(@RequestParam String username, @RequestParam String password) { 
        User userTmp=userRepository.findByUsernameAndPassword(username, password);
        if(userTmp != null) {
           return "success";
        }
        else
        	return "failed";
    }
    
}
