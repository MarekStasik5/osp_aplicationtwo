package com.stach.model;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="firefighter")
@NamedQueries({
@NamedQuery(name="Firefighter.showByFireProfession",
			query="SELECT e FROM Firefighter e WHERE e.profession=:profession"), 
			})
public class Firefighter {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_firefighter")
	private Long idFirefighter;
	@Column(nullable = false)
	private String firstName;
	@Column(nullable = false)
	private String lastName;
	private int age;
	private String profession;
	private String eligibility;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
	private User user;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@Fetch(FetchMode.SELECT)
	@JoinTable(name="firefighter_incident",
	joinColumns = {@JoinColumn(name="firefighter_id", referencedColumnName="id_firefighter")},
    inverseJoinColumns = {@JoinColumn(name="incident_id", referencedColumnName="id_incident")})
	private Set<Incident> incidents;
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "unit_id")
	private Unit unit;
	
	public void addIncident(Incident incident) {
		if(!incidents.contains(incident)) {
			incidents.add(incident);
			incident.addFirefighter(this);
		}
	}
	
	public Firefighter() {}

	public Firefighter(String firstName, String lastName, int age, String profession, String eligibility) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.profession = profession;
		this.eligibility = eligibility;
	}

	public Long getIdFirefighter() {
		return idFirefighter;
	}

	public void setIdFirefighter(Long idFirefighter) {
		this.idFirefighter = idFirefighter;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getEligibility() {
		return eligibility;
	}

	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public Set<Incident> getIncidents() {
		return incidents;
	}

	public void setIncidents(Set<Incident> incidents) {
		this.incidents = incidents;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Firefighter [idFirefighter=" + idFirefighter + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", age=" + age + ", profession=" + profession + ", eligibility=" + eligibility + ", user=" + user
				+ ", incidents=" + incidents + ", unit=" + unit + "]";
	}		

}
