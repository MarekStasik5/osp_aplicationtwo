package com.stach.model;

import java.util.Set;

import javax.persistence.*;

@Entity
public class Unit {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_unit")
	private Long idUnit;
	@Column(nullable = false)
	private String city;
	private String truck;
	@OneToMany(mappedBy = "unit", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<Firefighter>firefighters;
	
	public void addFirefighter(Firefighter firefighter) {
		firefighter.setUnit(this);
		getFirefighters().add(firefighter);
	}
	
	public Unit() {}
	
	public Unit(String city, String truck) {
		this.city = city;
		this.truck = truck;
	}
	public Long getIdUnit() {
		return idUnit;
	}
	public void setIdUnit(Long idUnit) {
		this.idUnit = idUnit;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getTruck() {
		return truck;
	}
	public void setTruck(String truck) {
		this.truck = truck;
	}

	public Set<Firefighter> getFirefighters() {
		return firefighters;
	}

	public void setFirefighters(Set<Firefighter> firefighters) {
		this.firefighters = firefighters;
	}

	@Override
	public String toString() {
		return "Unit [idUnit=" + idUnit + ", city=" + city + ", truck=" + truck + ", firefighters=" + firefighters
				+ "]";
	}


	
	
}
