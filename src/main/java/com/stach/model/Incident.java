package com.stach.model;

import java.util.Set;
import javax.persistence.*;

@Entity
public class Incident {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_incident")
	private Long idIncident;
	@Column(nullable = false)
	private String place;
	@Column(name = "type",nullable = false)
	private String typeOfIncident;
	@ManyToMany(mappedBy = "incidents")
    private Set<Firefighter> firefighters;
	
	public void addFirefighter(Firefighter firefighter) {
		if(!firefighters.contains(firefighter)) {
			firefighters.add(firefighter);
			firefighter.addIncident(this);
		}
	}
	
	public Incident() {}
	
	public Incident(String place, String typeOfIncident) {
		this.place = place;
		this.typeOfIncident = typeOfIncident;
	}
	public Long getIdIncident() {
		return idIncident;
	}
	public void setIdIncident(Long idIncident) {
		this.idIncident = idIncident;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getTypeOfIncident() {
		return typeOfIncident;
	}
	public void setTypeOfIncident(String typeOfIncident) {
		this.typeOfIncident = typeOfIncident;
	}

	public Set<Firefighter> getFirefighters() {
		return firefighters;
	}

	public void setFirefighters(Set<Firefighter> firefighters) {
		this.firefighters = firefighters;
	}

	@Override
	public String toString() {
		return "Incident [idIncident=" + idIncident + ", place=" + place + ", typeOfIncident=" + typeOfIncident
				+ ", firefighters=" + firefighters + "]";
	}



		
}
