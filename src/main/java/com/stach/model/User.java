package com.stach.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_user")
	private Long idUser;
	@Column(nullable = false)
	private String username;
	@Column(nullable = false)
	private String password;
	@OneToOne(mappedBy="user")
	//@OneToOne
	private Firefighter firefigher;

	
	public User() {}

	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Firefighter getFirefigher() {
		return firefigher;
	}

	public void setFirefigher(Firefighter firefigher) {
		this.firefigher = firefigher;
	}

	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", username=" + username + ", password=" + password + ", firefigher="
				+ firefigher + "]";
	}
	
}
