package com.stach.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stach.model.Unit;

@Repository
public interface UnitRepository extends JpaRepository<Unit,Long> {
	Unit findByCity(String city);
	List<Unit> findByTruck(String truck);
}
