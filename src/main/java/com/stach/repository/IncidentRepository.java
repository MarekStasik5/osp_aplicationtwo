package com.stach.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stach.model.Incident;

@Repository
public interface IncidentRepository extends JpaRepository<Incident,Long> {

}
