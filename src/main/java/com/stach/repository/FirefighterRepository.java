package com.stach.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.stach.model.Firefighter;

@Repository
public interface FirefighterRepository extends JpaRepository<Firefighter,Long> {
	Firefighter findByLastName(String lastName);
	
	@Query(name="Firefighter.showByFireProfession")
	List<Firefighter>showByFireProfession(@Param("profession") String profession);
	
	List<Firefighter> findByUnit_City(String city);
	
}
